package com.weixin.msg;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.weixin.vo.Video;

@XmlRootElement(name="xml")
public class VideoMsg extends Msg {
	private Video video;
	public VideoMsg() {
		// TODO Auto-generated constructor stub
		this.msgType=MsgEmum.video.toString();
	}
	@XmlElement(name="Video")
	public Video getVideo() {
		return video;
	}

	public void setVideo(Video video) {
		this.video = video;
	}
}

package com.vo;
/**
 * ArticleTypeVo.java
 * @author  microxdd
 * @version 创建时间：2014 2014年9月13日 下午3:22:04 
 * micrxdd
 * 
 */
public class NewsTypeVo {

    private Integer id;
    private String name;
    private Integer pid;
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Integer getPid() {
        return pid;
    }
    public void setPid(Integer pid) {
        this.pid = pid;
    }
}

package com.dto;

import java.sql.Timestamp;
public class TextDto {
    private Integer id;
    private String text;
    private Timestamp time;
    private Timestamp edittime;
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }
    public Timestamp getTime() {
        return time;
    }
    public void setTime(Timestamp time) {
        this.time = time;
    }
    public Timestamp getEdittime() {
        return edittime;
    }
    public void setEdittime(Timestamp edittime) {
        this.edittime = edittime;
    }
    
}

package com.dto;

/**
 * 消息规则数据传输对象
 * MsgRoleDto.java
 * @author  microxdd
 * @time 创建时间：2014 2014年9月13日 下午10:28:24 
 * @version
 * @user micrxdd
 */
public class MsgRoleDto {
    private Integer id;
    private Integer prsid;
    private String prsname;
    private String rtype;
    private String event;
    private String eventkey;
    private Boolean enable;
    
    public Boolean getEnable() {
        return enable;
    }
    public void setEnable(Boolean enable) {
        this.enable = enable;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getPrsid() {
        return prsid;
    }
    public void setPrsid(Integer prsid) {
        this.prsid = prsid;
    }
    
    public String getPrsname() {
        return prsname;
    }
    public void setPrsname(String prsname) {
        this.prsname = prsname;
    }
    public String getRtype() {
        return rtype;
    }
    public void setRtype(String rtype) {
        this.rtype = rtype;
    }
    public String getEvent() {
        return event;
    }
    public void setEvent(String event) {
        this.event = event;
    }
    public String getEventkey() {
        return eventkey;
    }
    public void setEventkey(String eventkey) {
        this.eventkey = eventkey;
    }
    
}

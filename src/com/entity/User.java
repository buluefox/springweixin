package com.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * User entity. @author MyEclipse Persistence Tools
 */

public class User implements java.io.Serializable {

    // Fields

    private Integer id;
    private Group group;
    private String username;
    private String password;
    private Timestamp creattime;
    private Set articles = new HashSet(0);
    private Set voiceses = new HashSet(0);
    private Set videoses = new HashSet(0);
    private Set texts = new HashSet(0);
    private Set newsitems = new HashSet(0);
    private Set musics = new HashSet(0);
    private Set imageses = new HashSet(0);
    private Set userRoles = new HashSet(0);

    // Constructors

    /** default constructor */
    public User() {
    }

    /** minimal constructor */
    public User(String username, String password) {
	this.username = username;
	this.password = password;
    }

    /** full constructor */
    public User(Group group, String username, String password,
	    Timestamp creattime, Set articles, Set voiceses, Set videoses,
	    Set texts, Set newsitems, Set musics, Set imageses, Set userRoles) {
	this.group = group;
	this.username = username;
	this.password = password;
	this.creattime = creattime;
	this.articles = articles;
	this.voiceses = voiceses;
	this.videoses = videoses;
	this.texts = texts;
	this.newsitems = newsitems;
	this.musics = musics;
	this.imageses = imageses;
	this.userRoles = userRoles;
    }

    // Property accessors

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	System.out.println("++++++++++++++++++++++++++++++++++有人动id了"+id);
	if(id!=null){
	    this.id = id;
	}
    }

    public Group getGroup() {
	return this.group;
    }

    public void setGroup(Group group) {
	this.group = group;
    }

    public String getUsername() {
	return this.username;
    }

    public void setUsername(String username) {
	this.username = username;
    }

    public String getPassword() {
	return this.password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public Timestamp getCreattime() {
	return this.creattime;
    }

    public void setCreattime(Timestamp creattime) {
	this.creattime = creattime;
    }

    public Set getArticles() {
	return this.articles;
    }

    public void setArticles(Set articles) {
	this.articles = articles;
    }

    public Set getVoiceses() {
	return this.voiceses;
    }

    public void setVoiceses(Set voiceses) {
	this.voiceses = voiceses;
    }

    public Set getVideoses() {
	return this.videoses;
    }

    public void setVideoses(Set videoses) {
	this.videoses = videoses;
    }

    public Set getTexts() {
	return this.texts;
    }

    public void setTexts(Set texts) {
	this.texts = texts;
    }

    public Set getNewsitems() {
	return this.newsitems;
    }

    public void setNewsitems(Set newsitems) {
	this.newsitems = newsitems;
    }

    public Set getMusics() {
	return this.musics;
    }

    public void setMusics(Set musics) {
	this.musics = musics;
    }

    public Set getImageses() {
	return this.imageses;
    }

    public void setImageses(Set imageses) {
	this.imageses = imageses;
    }

    public Set getUserRoles() {
	return this.userRoles;
    }

    public void setUserRoles(Set userRoles) {
	this.userRoles = userRoles;
    }

}
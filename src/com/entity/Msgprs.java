package com.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * 消息分发处理类
 * Msgprs entity.
 * @author micrxdd
 *
 */

public class Msgprs implements java.io.Serializable {

    // Fields
    private Integer id;
    private String name;
    private String retype;
    private Integer msgId;
    private Timestamp stime;
    private Timestamp endtime;
    private String plugin;
    private Boolean enable;
    private Set msgroles = new HashSet(0);
    private Set textroles = new HashSet(0);

    // Constructors

    /** default constructor */
    public Msgprs() {
    }

    /** full constructor */
    public Msgprs(String name, String retype, Integer msgId, Timestamp stime,
	    Timestamp endtime, String plugin, Boolean enable, Set msgroles,
	    Set textroles) {
	this.name = name;
	this.retype = retype;
	this.msgId = msgId;
	this.stime = stime;
	this.endtime = endtime;
	this.plugin = plugin;
	this.enable = enable;
	this.msgroles = msgroles;
	this.textroles = textroles;
    }

    // Property accessors

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getName() {
	return this.name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getRetype() {
	return this.retype;
    }

    public void setRetype(String retype) {
	this.retype = retype;
    }

    public Integer getMsgId() {
	return this.msgId;
    }

    public void setMsgId(Integer msgId) {
	this.msgId = msgId;
    }

    public Timestamp getStime() {
	return this.stime;
    }

    public void setStime(Timestamp stime) {
	this.stime = stime;
    }

    public Timestamp getEndtime() {
	return this.endtime;
    }

    public void setEndtime(Timestamp endtime) {
	this.endtime = endtime;
    }

    public String getPlugin() {
	return this.plugin;
    }

    public void setPlugin(String plugin) {
	this.plugin = plugin;
    }

    public Boolean getEnable() {
	return this.enable;
    }

    public void setEnable(Boolean enable) {
	this.enable = enable;
    }

    public Set getMsgroles() {
	return this.msgroles;
    }

    public void setMsgroles(Set msgroles) {
	this.msgroles = msgroles;
    }

    public Set getTextroles() {
	return this.textroles;
    }

    public void setTextroles(Set textroles) {
	this.textroles = textroles;
    }

}
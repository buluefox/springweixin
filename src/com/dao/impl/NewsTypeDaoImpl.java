package com.dao.impl;

import org.springframework.stereotype.Repository;

import com.dao.NewsTypeDao;
import com.entity.Newstype;
@Repository("newsTypeDao")
public class NewsTypeDaoImpl extends BaseDaoImpl<Newstype> implements NewsTypeDao{
    
}

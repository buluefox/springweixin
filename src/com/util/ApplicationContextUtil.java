package com.util;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
@Component("applicationContextUtil")
public class ApplicationContextUtil implements ApplicationContextAware{
	public ApplicationContextUtil() {
		// TODO Auto-generated constructor stub
		System.out.println(this +"init");
	}
	private ApplicationContext applicationContext;
	private DefaultListableBeanFactory beanFactory;
	public void setApplicationContext(ApplicationContext arg0)
			throws BeansException {
		// TODO Auto-generated method stub
		applicationContext=arg0;
		beanFactory = (DefaultListableBeanFactory) arg0.getAutowireCapableBeanFactory();
	}
	public DefaultListableBeanFactory getBeanFactory() {
		return beanFactory;
	}
	public void setBeanFactory(DefaultListableBeanFactory beanFactory) {
		this.beanFactory = beanFactory;
	}
	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}
	
	

}

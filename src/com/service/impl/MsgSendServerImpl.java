package com.service.impl;

import java.sql.Timestamp;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.dao.ImageDao;
import com.dao.NewsDao;
import com.dao.NewsItemDao;
import com.dao.TextDao;
import com.dao.VideoDao;
import com.dao.VoiceDao;
import com.entity.Images;
import com.entity.Msgprs;
import com.entity.News;
import com.entity.Newsitem;
import com.entity.Text;
import com.entity.Videos;
import com.entity.Voices;
import com.service.ConfigService;
import com.service.MsgPlueService;
import com.service.MsgSendServer;
import com.weixin.msg.ArticleAndPicsMsg;
import com.weixin.msg.ImageMsg;
import com.weixin.msg.MsgEmum;
import com.weixin.msg.TextMsg;
import com.weixin.msg.VideoMsg;
import com.weixin.msg.VoiceMsg;
import com.weixin.plugin.MsgPlugin;
import com.weixin.util.WeixinMsg;
import com.weixin.vo.Video;
/**
 * 消息发送服务
 * MsgSendServerImpl.java
 * @author  microxdd
 * @time 创建时间：2014 2014年9月14日 下午12:45:30 
 * @version
 * @user micrxdd
 */
@Service("msgSendServer")
public class MsgSendServerImpl implements MsgSendServer {
    private final static Logger _log = Logger.getLogger(MsgServiceImpl.class);
    @Resource
    private TextDao textDao;
    @Resource
    private VideoDao videoDao;
    @Resource
    private VoiceDao voiceDao;
    @Resource
    private ImageDao imageDao;
    @Resource
    private NewsDao newsDao;
    @Resource
    private NewsItemDao newsItemDao;
    @Resource
    private MsgPlueService msgPlueService;
    @Resource
    private ConfigService configService;
    /**
     * 发送消息
     */
    @Override
    public Object SendMsg(Msgprs msgprs, Map<String, String> msg) {
	// TODO Auto-generated method stub
	if(msgprs==null)return null;
	System.out.println("开始发送消息:>>>消息处理器"+msgprs.getName());
	if (msgprs.getEnable()) {
	    return SendMsgByPlugin(msgprs, msg);
	}
	Timestamp timestamp=new Timestamp(System.currentTimeMillis());
	if(timestamp.after(msgprs.getStime())&&timestamp.before(msgprs.getEndtime())){
	    MsgEmum emum = MsgEmum.valueOf(msgprs.getRetype());
		switch (emum) {
		case text:
		    return onTextMsg(msgprs, msg);
		case video:
		    return onVideoMsg(msgprs, msg);
		case voice:
		    return onVoiceMsg(msgprs, msg);
		case image:
		    return onImageMsg(msgprs, msg);
		case news:
		    return onNewsMsg(msgprs, msg);
		default:
		    System.out.println("返回默认消息");
		    return SendDefaultMsg(msg);
		}
	}
	return null;
    }

    /**
     * 返回文本消息
     * 
     * @param msgprs
     * @param msg
     * @return
     */
    public Object onTextMsg(Msgprs msgprs, Map<String, String> msg) {
	System.out.println("开始返回文本消息"+"文本消息id:"+msgprs.getMsgId());
	Text text = textDao.findById(msgprs.getMsgId());
	// 没有找到消息
	if (text == null)return null;
	TextMsg tmsg = new TextMsg();
	tmsg.setContent(text.getText());
	tmsg.setFromUserName(msg.get(WeixinMsg.ToUserName));
	tmsg.setToUserName(msg.get(WeixinMsg.FromUserName));
	return tmsg;
    }

    /**
     * 返回新闻消息
     * 
     * @param msgprs
     * @param msg
     * @return
     */
    public Object onNewsMsg(Msgprs msgprs, Map<String, String> msg) {
	System.out.println("================");
	Integer id = msgprs.getMsgId();
	Newsitem newsitem = null;
	if (id == null) {
	    newsitem = newsItemDao.getLast();
	} else {
	    newsitem = newsItemDao.findById(id);
	}
	if (newsitem == null||newsitem.getNewses().size()==0)
	    return null;
	// Set<Newsitem> set= newsitem.getNewses();
	System.out.println("获取到新闻消息 id:"+newsitem.getId());
	Set<News> news = newsitem.getNewses();
	ArticleAndPicsMsg andPicsMsg = new ArticleAndPicsMsg();
	for (News news2 : news) {
	    com.weixin.vo.Article article = new com.weixin.vo.Article();
	    BeanUtils.copyProperties(news2, article,"picUrl","url");
	    String picUrl=news2.getPicUrl();
	    if(!picUrl.startsWith("http")){
		article.setPicUrl(configService.findUrl()+picUrl);
	    }else{
		article.setPicUrl(picUrl);
	    }
	    String url;
	    if(news2.getArticleid()!=null){
		url=configService.findUrl()+"article/"+news2.getArticleid()+".htm";
	    }else{
		url=news2.getUrl();
	    }
	    article.setUrl(url);
	    andPicsMsg.AddArticles(article);
	}
	andPicsMsg.setFromUserName(msg.get(WeixinMsg.ToUserName));
	andPicsMsg.setToUserName(msg.get(WeixinMsg.FromUserName));
	return andPicsMsg;
    }

    /**
     * 通过插件返回消息
     * 
     * @param msgprs
     * @param msg
     * @return
     */
    public Object SendMsgByPlugin(Msgprs msgprs, Map<String, String> msg) {
	String plugin = msgprs.getPlugin();
	if (plugin != null && !plugin.equals("")) {
	    MsgPlugin server = (MsgPlugin) msgPlueService
		    .getPlugin(plugin);
	    if (server != null) {
		_log.info("发送插件消息");
		return server.returnMsg(msg);
	    }
	}
	return null;

    }

    /**
     * 返回视频消息
     * 
     * @param msgprs
     * @param msg
     * @return
     */
    public Object onVideoMsg(Msgprs msgprs, Map<String, String> msg) {
	VideoMsg videoMsg = new VideoMsg();
	Video video = new Video();
	Videos videos = videoDao.findById(msgprs.getMsgId());
	videos = videos == null ? videoDao.getLast() : videos;
	if (videos == null)
	    return null;
	BeanUtils.copyProperties(videos, video);
	videoMsg.setVideo(video);
	videoMsg.setFromUserName(msg.get(WeixinMsg.ToUserName));
	videoMsg.setToUserName(msg.get(WeixinMsg.FromUserName));
	return videoMsg;
    }

    /**
     * 返回图片消息
     * 
     * @param msgprs
     * @param msg
     * @return
     */
    public Object onImageMsg(Msgprs msgprs, Map<String, String> msg) {
	ImageMsg imageMsg = new ImageMsg();
	Images images = imageDao.findById(msgprs.getMsgId());
	images = images == null ? imageDao.getLast() : images;
	if (images == null)
	    return null;
	imageMsg.AddMediaId(images.getMediaid().getMediaid());
	imageMsg.setFromUserName(msg.get(WeixinMsg.ToUserName));
	imageMsg.setToUserName(msg.get(WeixinMsg.FromUserName));
	return imageMsg;
    }

    /**
     * 返回语音消息
     * 
     * @param msgprs
     * @param msg
     * @return
     */
    public Object onVoiceMsg(Msgprs msgprs, Map<String, String> msg) {
	VoiceMsg voiceMsg = new VoiceMsg();
	Voices voices = voiceDao.findById(msgprs.getMsgId());
	voiceMsg.AddMediaId(voices.getMediaid().getMediaid());
	voiceMsg.setFromUserName(msg.get(WeixinMsg.ToUserName));
	voiceMsg.setToUserName(msg.get(WeixinMsg.FromUserName));
	return voiceMsg;
    }

    /**
     * 返回默认消息
     * 
     * @param msg
     * @return
     */
    @Override
    public Object SendDefaultMsg(Map<String, String> msg) {
	// TODO Auto-generated method stub
	_log.info("发送默认消息");
	TextMsg msg1 = new TextMsg();
	msg1.setContent("默认返回消息");
	msg1.setFromUserName(msg.get(WeixinMsg.ToUserName));
	msg1.setToUserName(msg.get(WeixinMsg.FromUserName));
	return msg1;
    }
}

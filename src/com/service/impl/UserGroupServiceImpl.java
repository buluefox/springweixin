package com.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.dao.UserGroupDao;
import com.dto.Pageinfo;
import com.dto.Pagers;
import com.dto.UserGroupDto;
import com.entity.Group;
import com.service.UserGroupService;
@Service("userGroupService")
public class UserGroupServiceImpl implements UserGroupService{
    @Resource
    private UserGroupDao userGroupDao;

    @Override
    public Pagers Groups(Pageinfo pageinfo) {
	// TODO Auto-generated method stub
	Pagers pagers=userGroupDao.getForPage(pageinfo);
	List<Group> groups=(List<Group>) pagers.getRows();
	List<UserGroupDto> dtos=new ArrayList<UserGroupDto>();
	for (Group group : groups) {
	    UserGroupDto userGroupDto=new UserGroupDto();
	    BeanUtils.copyProperties(group, userGroupDto);
	    dtos.add(userGroupDto);
	}
	pagers.setRows(dtos);
	return pagers;
    }

    @Override
    public void SaveGroups(List<UserGroupDto> dtos) {
	// TODO Auto-generated method stub
	for (UserGroupDto userGroupDto : dtos) {
	    Group group=new Group();
	    BeanUtils.copyProperties(userGroupDto, group,"id");
	    userGroupDao.save(group);
	}
    }

    @Override
    public void UpdateGroups(List<UserGroupDto> dtos) {
	// TODO Auto-generated method stub
	for (UserGroupDto userGroupDto : dtos) {
	    if(userGroupDto.getId()<0){
		Group group=new Group();
		    BeanUtils.copyProperties(userGroupDto, group,"id");
		    userGroupDao.save(group);
	    }else{
		Group group=userGroupDao.findById(userGroupDto.getId());
		BeanUtils.copyProperties(userGroupDto, group,"id");
	    }
	}
    }

    @Override
    public void DelGroups(Integer[] ids) {
	// TODO Auto-generated method stub
	for (Integer id : ids) {
	    if(id!=1){
		userGroupDao.DelById(id);
	    }
	}
    }

    @Override
    public void UpdateRole(Integer groupid, String ids) {
	// TODO Auto-generated method stub
	Group group=userGroupDao.findById(groupid);
	group.setMenusids(ids);
    }
    
}

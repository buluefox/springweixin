package com.service.impl;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import com.service.MsgPlueService;
@Service("msgPlueService")
public class MsgPlueServiceImpl implements MsgPlueService,ApplicationContextAware {
	/*private final static Logger _log = Logger.getLogger(MsgPlueServiceImpl.class);
	private DefaultListableBeanFactory beanFactory;*/
	private ApplicationContext context;
	public void setApplicationContext(ApplicationContext arg0)
			throws BeansException {
		// TODO Auto-generated method stub
		this.context=arg0;
	}
	public <T> T getPlugin(Class<T> t) {
		// TODO Auto-generated method stub
		return context.getBean(t);
	}

	public Object getPlugin(String name) {
		// TODO Auto-generated method stub
		return context.getBean(name);
	}
	
}

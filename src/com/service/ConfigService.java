package com.service;

import com.entity.Config;

/**
 * ConfigService.java
 * @author  microxdd
 * @version 创建时间：2014 2014年9月20日 下午1:58:00 
 * micrxdd
 * 
 */
public interface ConfigService {
    /**
     * 初始化配置
     */
    public void initConfig();
    /**
     * 返回网站配置的url 因为网站域名ip可能会变化，所以提供了这一个配置
     * @return String的url
     */
    public String findUrl();
    /**
     * 更新配置
     * @param key
     * @param value
     */
    public void Update(String key,String value);
    /**
     * 保存配置用户自定义配置
     * @param key
     * @param value
     */
    public void SaveConfig(String key,String value);
    
}
